-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 07 juil. 2023 à 09:20
-- Version du serveur : 10.6.14-MariaDB
-- Version de PHP : 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sc1viem4645_area_dev`
--

-- --------------------------------------------------------

--
-- Structure de la table `Administres_Reseaux_ejcj9`
--

CREATE TABLE `Administres_Reseaux_ejcj9` (
  `Id` char(36) NOT NULL,
  `CollectiviteId` char(36) DEFAULT NULL,
  `Facebook` varchar(500) DEFAULT NULL,
  `Twitter` varchar(500) DEFAULT NULL,
  `Instagram` varchar(500) DEFAULT NULL,
  `Snapchat` varchar(500) DEFAULT NULL,
  `Youtube` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Administres_Theme_6pn3k`
--

CREATE TABLE `Administres_Theme_6pn3k` (
  `Principale` char(6) NOT NULL,
  `Secondaire` char(6) NOT NULL,
  `Tertiaire` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Administres_Utilisateurs_g6o3f`
--

CREATE TABLE `Administres_Utilisateurs_g6o3f` (
  `ContactId` char(36) NOT NULL,
  `Identifiant` varchar(100) NOT NULL,
  `MotDePasse` varchar(250) NOT NULL,
  `CollectiviteId` char(36) DEFAULT NULL,
  `token` varchar(2000) NOT NULL,
  `IdMotDePasseOublie` char(36) DEFAULT NULL,
  `DateMotDePasseOublie` datetime DEFAULT NULL,
  `Actif` tinyint(4) DEFAULT NULL,
  `CGU` tinyint(4) DEFAULT NULL,
  `isVerifie` tinyint(4) DEFAULT NULL,
  `Admin` tinyint(4) DEFAULT NULL,
  `Bandeau` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `champs_mz3p7`
--

CREATE TABLE `champs_mz3p7` (
  `Type` char(20) NOT NULL,
  `Label` varchar(100) NOT NULL,
  `IdHtml` char(25) NOT NULL,
  `Ordre` int(3) NOT NULL,
  `param` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `champs_perso_l8f5x`
--

CREATE TABLE `champs_perso_l8f5x` (
  `Code` int(3) NOT NULL,
  `CollectiviteId` char(36) NOT NULL,
  `PageCode` varchar(50) NOT NULL,
  `IdHtmlName` varchar(50) NOT NULL,
  `Libelle` varchar(60) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `Ordre` int(2) NOT NULL,
  `Obligatoire` int(1) NOT NULL DEFAULT 1,
  `Actif` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `collectivite`
--

CREATE TABLE `collectivite` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(500) DEFAULT NULL,
  `Siret` char(15) DEFAULT NULL,
  `CodePostal` char(5) NOT NULL,
  `ImgCollectivite` varchar(500) DEFAULT NULL,
  `couleurPrincipale` char(6) DEFAULT NULL,
  `couleurSecondaire` char(6) DEFAULT NULL,
  `couleurTertiaire` char(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `collectivite_demarches`
--

CREATE TABLE `collectivite_demarches` (
  `Id` char(36) NOT NULL,
  `CollectiviteId` char(36) NOT NULL,
  `DemarcheCode` tinyint(3) NOT NULL,
  `Actif` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contact_historiques_ds78f`
--

CREATE TABLE `contact_historiques_ds78f` (
  `UserId` varchar(36) NOT NULL,
  `AncienneValeur` varchar(50) DEFAULT NULL,
  `NouvelleValeur` varchar(50) DEFAULT NULL,
  `ContactId` varchar(36) NOT NULL,
  `Date` datetime NOT NULL,
  `IsPrive` int(1) NOT NULL DEFAULT 0,
  `NomDuChamp` varchar(50) DEFAULT NULL,
  `Message` varchar(300) DEFAULT NULL,
  `IsMessage` int(1) NOT NULL DEFAULT 0,
  `IsInterne` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `contact_urzkc`
--

CREATE TABLE `contact_urzkc` (
  `Id` char(36) NOT NULL,
  `TypeCode` int(10) DEFAULT NULL,
  `Siret` varchar(17) DEFAULT NULL,
  `RaisonSociale` char(100) DEFAULT '',
  `Nom` varchar(200) DEFAULT NULL,
  `Prenom` varchar(40) DEFAULT NULL,
  `Mail` char(100) DEFAULT NULL,
  `Representant` varchar(100) DEFAULT NULL,
  `Adresse` varchar(200) DEFAULT NULL,
  `CplAdresse` char(50) DEFAULT NULL,
  `Ville` varchar(100) DEFAULT NULL,
  `Departement` varchar(3) DEFAULT NULL,
  `Telephone` varchar(14) DEFAULT NULL,
  `DateAjout` datetime DEFAULT NULL,
  `CreePar` varchar(36) NOT NULL,
  `CodePostal` varchar(6) DEFAULT NULL,
  `Score` varchar(20) DEFAULT NULL,
  `Coordonees` varchar(50) DEFAULT NULL,
  `Statut` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `droit_d21cf`
--

CREATE TABLE `droit_d21cf` (
  `Code` varchar(20) NOT NULL,
  `Libelle` varchar(100) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `droit_d21cf`
--

INSERT INTO `droit_d21cf` (`Code`, `Libelle`, `Description`) VALUES
('ADMIN', 'Administration', 'Permet d\'accéder au menu d\'administration'),
('ADMIN_DRT', 'Administration des droits', 'Permet d\'accéder au menu d\'administration des droits'),
('ADMIN_LST', 'Administration des listes', 'Permet d\'accéder au menu d\'administration des listes'),
('ADMIN_USR', 'Administration des utilisateurs', 'Permet d\'accéder au menu d\'administration des utilisateurs'),
('ADMIN_VAR', 'Administration des Variables personnalisées', 'Permet d\'accéder au menu d\'administration des variables personnalisées'),
('ALL_SOLLI', 'Toutes les sollicitations', 'Permet d\'avoir accès à toutes les sollicitations'),
('CREAT_CONT', 'Création de contact', 'Permet de donner les droits de créer un nouveau contact'),
('EXPORT', 'Export', 'Permet de faire des exports de la base de sollicitation'),
('MODIF_CONT', 'Modification des contacts', 'Permet de donner les droits de modifier un contact'),
('USER_STAT', 'Statistiques par utilisateur', 'Permet de faire des statistiques par utilisateur'),
('VALID_CONT', 'Validation de contact', 'Permet de valider l\'ajout d\'un contact');

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE `etat` (
  `Id` char(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `Libelle` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`Id`, `Libelle`) VALUES
('1', 'Programmé'),
('2', 'Reprogrammé'),
('3', 'Déplacé en ligne'),
('4', 'Reporté'),
('5', 'Complet'),
('6', 'Annulé');

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `Id` char(36) NOT NULL,
  `Titre` varchar(150) NOT NULL,
  `Img` varchar(500) DEFAULT NULL,
  `CreditImg` varchar(255) DEFAULT NULL,
  `DescriptionCourte` varchar(210) NOT NULL,
  `DescriptionLongue` longtext DEFAULT NULL,
  `MotsCles` varchar(255) DEFAULT NULL,
  `ConditionParticipation` varchar(260) DEFAULT NULL,
  `Outils` varchar(255) DEFAULT NULL,
  `AgeMax` int(3) NOT NULL DEFAULT 99,
  `AgeMin` int(3) NOT NULL DEFAULT 1,
  `Auditif` tinyint(1) NOT NULL DEFAULT 0,
  `Visuel` tinyint(1) NOT NULL DEFAULT 0,
  `Psychique` tinyint(1) NOT NULL DEFAULT 0,
  `Moteur` tinyint(1) NOT NULL DEFAULT 0,
  `Intellectuel` tinyint(1) NOT NULL DEFAULT 0,
  `LieuxId` char(36) NOT NULL,
  `Horaires` varchar(5000) NOT NULL,
  `Organisateur` char(36) NOT NULL,
  `EtatId` char(36) NOT NULL,
  `StatutId` char(36) NOT NULL,
  `Limite` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `groupe_droit_g2l3i`
--

CREATE TABLE `groupe_droit_g2l3i` (
  `GroupeCode` varchar(20) DEFAULT NULL,
  `DroitCode` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `groupe_droit_g2l3i`
--

INSERT INTO `groupe_droit_g2l3i` (`GroupeCode`, `DroitCode`) VALUES
('1', 'CREAT_COLL'),
('1', 'MODIF_SENSIBLE'),
('2', 'ADMIN'),
('2', 'ADMIN_DRT'),
('4', 'ADMIN'),
('4', 'ADMIN_DRT'),
('4', 'ADMIN_LST'),
('1', 'ADMIN_LST'),
('1', 'MODIF_COLL'),
('1', 'MODIF_PB_FACT'),
('1', 'ADMIN'),
('5', 'ADMIN'),
('1', 'ADMIN_VAR'),
('2', 'MODIF_SENSIBLE'),
('2', 'MODIF_PB_FACT'),
('2', 'USER_STAT'),
('2', 'MODIF_COLL'),
('2', 'EXPORT'),
('2', 'CREAT_COLL'),
('2', 'ADMIN_VAR'),
('2', 'ADMIN_USR'),
('2', 'ADMIN_LST'),
('1', 'CREAT_CONT'),
('1', 'MODIF_CONT'),
('2', 'CREAT_CONT'),
('2', 'MODIF_CONT'),
('1', 'VALID_CONT'),
('7', 'VALID_CONT'),
('1', 'ALL_SOLLI'),
('1', 'USER_STAT'),
('1', 'EXPORT'),
('1', 'ADMIN_USR'),
('10', 'ADMIN'),
('10', 'VALID_CONT'),
('10', 'USER_STAT'),
('10', 'MODIF_CONT'),
('10', 'EXPORT'),
('10', 'ALL_SOLLI'),
('10', 'CREAT_CONT'),
('10', 'ADMIN_VAR'),
('10', 'ADMIN_USR'),
('10', 'ADMIN_LST'),
('10', 'ADMIN_DRT'),
('10', 'ADMIN'),
('1', 'ADMIN_DRT');

-- --------------------------------------------------------

--
-- Structure de la table `groupe_g1pk5`
--

CREATE TABLE `groupe_g1pk5` (
  `Code` int(10) NOT NULL,
  `Libelle` varchar(100) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `groupe_utilisateur`
--

CREATE TABLE `groupe_utilisateur` (
  `GroupeCode` varchar(20) DEFAULT NULL,
  `UtilisateurId` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `groupe_utilisateur_g32tr`
--

CREATE TABLE `groupe_utilisateur_g32tr` (
  `GroupeCode` varchar(20) DEFAULT NULL,
  `UtilisateurId` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `inscription_evenement`
--

CREATE TABLE `inscription_evenement` (
  `Id` char(36) NOT NULL,
  `EvenementId` char(36) DEFAULT NULL,
  `ContactId` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `interlocuteurs_ry7a9`
--

CREATE TABLE `interlocuteurs_ry7a9` (
  `Id` char(36) NOT NULL,
  `ContactId` char(36) NOT NULL,
  `Nom` varchar(50) DEFAULT NULL,
  `Prenom` varchar(50) DEFAULT NULL,
  `Telephone` varchar(14) DEFAULT NULL,
  `Titre` char(5) DEFAULT NULL,
  `Mail` varchar(200) DEFAULT NULL,
  `Commentaire` varchar(500) DEFAULT NULL,
  `LienContactId` varchar(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `json_page_c4sd6`
--

CREATE TABLE `json_page_c4sd6` (
  `Id` int(6) NOT NULL,
  `IdCible` varchar(36) NOT NULL,
  `Page` varchar(20) NOT NULL,
  `Json` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liens_externes`
--

CREATE TABLE `liens_externes` (
  `Id` char(36) NOT NULL,
  `CollectiviteId` char(36) NOT NULL,
  `Type` varchar(100) NOT NULL,
  `Nom` varchar(200) DEFAULT NULL,
  `Url` varchar(200) DEFAULT NULL,
  `Description` varchar(200) DEFAULT NULL,
  `Actif` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `lieux`
--

CREATE TABLE `lieux` (
  `Id` char(36) NOT NULL,
  `Pays` varchar(100) NOT NULL,
  `adresse` varchar(1000) NOT NULL,
  `CP` int(10) NOT NULL,
  `Img` varchar(500) DEFAULT NULL,
  `CreditImg` varchar(1000) DEFAULT NULL,
  `Detail` longtext DEFAULT NULL,
  `ConditionAcces` varchar(2000) DEFAULT NULL,
  `Tel` char(20) DEFAULT NULL,
  `Site` varchar(500) DEFAULT NULL,
  `Mail` varchar(200) DEFAULT NULL,
  `Nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `liste_champs_bn45z`
--

CREATE TABLE `liste_champs_bn45z` (
  `Code` varchar(50) NOT NULL,
  `Libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `liste_champs_bn45z`
--

INSERT INTO `liste_champs_bn45z` (`Code`, `Libelle`) VALUES
('datepicker', 'Date'),
('multiselect', 'Liste à choix multiple'),
('select', 'Liste'),
('text', 'Texte court'),
('textarea', 'Texte Long');

-- --------------------------------------------------------

--
-- Structure de la table `liste_perso_b2g5n`
--

CREATE TABLE `liste_perso_b2g5n` (
  `Code` int(4) NOT NULL,
  `CodeChamp` int(4) NOT NULL,
  `Libelle` varchar(30) NOT NULL,
  `Ordre` int(2) NOT NULL,
  `Actif` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mail`
--

CREATE TABLE `mail` (
  `Id` char(36) NOT NULL,
  `Destinataire` varchar(500) NOT NULL,
  `HtmlContenu` text NOT NULL,
  `Objet` varchar(500) NOT NULL,
  `Date` datetime NOT NULL,
  `IdUtilisateur` char(36) NOT NULL,
  `IdSollicitation` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `notification_her7u`
--

CREATE TABLE `notification_her7u` (
  `UtilisateurId` char(36) NOT NULL,
  `SujetId` char(36) NOT NULL,
  `Date` datetime DEFAULT NULL,
  `NotificationType` tinyint(3) UNSIGNED DEFAULT NULL,
  `IsLu` tinyint(1) DEFAULT NULL,
  `IsVu` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `page_champ_tr1y2`
--

CREATE TABLE `page_champ_tr1y2` (
  `Code` varchar(50) NOT NULL,
  `Libelle` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `page_champ_tr1y2`
--

INSERT INTO `page_champ_tr1y2` (`Code`, `Libelle`) VALUES
('SOLLICITATION', 'Sollicitations'),
('CONTACT', 'Contacts');

-- --------------------------------------------------------

--
-- Structure de la table `preference_d5z8y`
--

CREATE TABLE `preference_d5z8y` (
  `UtilisateurId` char(36) NOT NULL,
  `Code` varchar(20) NOT NULL,
  `Json` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ref_demandes`
--

CREATE TABLE `ref_demandes` (
  `Code` tinyint(3) NOT NULL,
  `Titre` char(50) NOT NULL,
  `Icon` char(50) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `Type` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `ref_demandes`
--

INSERT INTO `ref_demandes` (`Code`, `Titre`, `Icon`, `Description`, `Type`) VALUES
(1, 'CARTE D\'IDENTITE', 'clipboard', 'Demandez ou consultez votre carte d\'identité', '1'),
(2, 'PASSEPORT', 'clipboard', 'Demandez ou consultez votre passeport', '2'),
(3, 'ACTE DE MARIAGE', 'envelope', 'Demandez ou consultez votre acte de mariage', '3'),
(4, 'ACTE DE NAISSANCE', 'copy', 'Demandez ou consultez votre acte de naissance', '3'),
(5, 'ACTE DE DÉCÈS', 'clipboard', 'Demandez ou consultez votre acte de décès', '3'),
(6, 'INSCRIPTION LISTES ÉLECTORALES', ' fas fa-clipboard-list', 'Inscrivez vous sur la liste électorale de votre commune', '3'),
(7, 'RECENSEMENT', ' fas fa-chalkboard-teacher', 'Bientôt 16 ans ? Inscrivez-vous au recensement', '3');

-- --------------------------------------------------------

--
-- Structure de la table `ref_etat_ph84p`
--

CREATE TABLE `ref_etat_ph84p` (
  `Code` int(11) NOT NULL,
  `Libelle` varchar(50) NOT NULL,
  `SysEtatCode` int(2) NOT NULL,
  `Actif` tinyint(1) NOT NULL DEFAULT 1,
  `Defaut` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `ref_etat_ph84p`
--

INSERT INTO `ref_etat_ph84p` (`Code`, `Libelle`, `SysEtatCode`, `Actif`, `Defaut`) VALUES
(1, 'Résolu', 4, 1, 0),
(2, 'Non résolu', 1, 1, 1),
(29, 'En cours', 2, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ref_objet_qs6v7`
--

CREATE TABLE `ref_objet_qs6v7` (
  `Code` int(11) NOT NULL,
  `Libelle` varchar(50) NOT NULL,
  `Actif` tinyint(1) NOT NULL DEFAULT 1,
  `Defaut` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ref_rendezvous_etat`
--

CREATE TABLE `ref_rendezvous_etat` (
  `Code` tinyint(3) NOT NULL,
  `Libelle` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `ref_rendezvous_etat`
--

INSERT INTO `ref_rendezvous_etat` (`Code`, `Libelle`) VALUES
(1, 'planifié'),
(2, 'en cours'),
(3, 'passé'),
(4, 'demandé'),
(5, 'annulé');

-- --------------------------------------------------------

--
-- Structure de la table `ref_rendezvous_objet`
--

CREATE TABLE `ref_rendezvous_objet` (
  `Code` tinyint(3) NOT NULL,
  `Libelle` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `ref_rendezvous_objet`
--

INSERT INTO `ref_rendezvous_objet` (`Code`, `Libelle`) VALUES
(1, 'Carte d\'identité'),
(2, 'Passeport'),
(3, 'Autre');

-- --------------------------------------------------------

--
-- Structure de la table `ref_service_qhm29`
--

CREATE TABLE `ref_service_qhm29` (
  `Code` int(11) NOT NULL,
  `Libelle` varchar(50) NOT NULL,
  `Actif` tinyint(1) NOT NULL DEFAULT 1,
  `Defaut` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ref_statut_zx8u7`
--

CREATE TABLE `ref_statut_zx8u7` (
  `Code` int(11) NOT NULL,
  `Libelle` varchar(50) NOT NULL,
  `Actif` tinyint(1) NOT NULL,
  `Defaut` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `ref_statut_zx8u7`
--

INSERT INTO `ref_statut_zx8u7` (`Code`, `Libelle`, `Actif`, `Defaut`) VALUES
(1, 'Bloquant', 1, 0),
(2, 'Normal', 1, 1),
(3, 'test', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ref_type_contact_ae4au`
--

CREATE TABLE `ref_type_contact_ae4au` (
  `Libelle` varchar(50) NOT NULL,
  `Code` char(10) NOT NULL,
  `Type` tinyint(1) NOT NULL,
  `Actif` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `ref_type_contact_ae4au`
--

INSERT INTO `ref_type_contact_ae4au` (`Libelle`, `Code`, `Type`, `Actif`) VALUES
('Entreprise', '1', 2, 1),
('Particulier', '2', 1, 1),
('CCAS', '3', 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `rendezvous`
--

CREATE TABLE `rendezvous` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(100) DEFAULT NULL,
  `ContactId` char(36) DEFAULT NULL,
  `ObjectCode` tinyint(3) DEFAULT NULL,
  `EtatCode` tinyint(3) DEFAULT NULL,
  `DateDebut` datetime DEFAULT NULL,
  `DateFin` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sib_contact_list_m1lk2`
--

CREATE TABLE `sib_contact_list_m1lk2` (
  `Id` int(11) NOT NULL,
  `Name` varchar(150) NOT NULL,
  `TotalSubscribers` int(11) NOT NULL,
  `TotalBlacklisted` int(11) NOT NULL,
  `FolderId` int(11) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `NbCompaign` int(11) NOT NULL,
  `DynamicList` tinyint(1) NOT NULL,
  `NbContacte` int(11) NOT NULL,
  `JsonFilter` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sollicitationhistoriques_ds45f`
--

CREATE TABLE `sollicitationhistoriques_ds45f` (
  `UserId` varchar(36) NOT NULL,
  `AncienneValeur` varchar(50) DEFAULT NULL,
  `NouvelleValeur` varchar(50) DEFAULT NULL,
  `SollicitationId` varchar(36) NOT NULL,
  `Date` datetime NOT NULL,
  `IsPrive` int(1) NOT NULL DEFAULT 0,
  `NomDuChamp` varchar(50) DEFAULT NULL,
  `Message` varchar(300) DEFAULT NULL,
  `IsMessage` int(1) NOT NULL DEFAULT 0,
  `IsInterne` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sollicitations_p55if`
--

CREATE TABLE `sollicitations_p55if` (
  `Id` varchar(36) NOT NULL,
  `ContactId` varchar(36) DEFAULT NULL,
  `DateCreation` datetime DEFAULT NULL,
  `CreePar` varchar(50) DEFAULT NULL,
  `DateResolution` datetime DEFAULT NULL,
  `Detail` varchar(2000) DEFAULT NULL,
  `EtatCode` varchar(20) DEFAULT NULL,
  `ServiceCode` varchar(30) DEFAULT NULL,
  `StatutCode` int(10) DEFAULT NULL,
  `ObjetCode` int(10) DEFAULT NULL,
  `Mail` varchar(100) DEFAULT NULL,
  `PersonneAffectee` varchar(100) DEFAULT NULL,
  `Telephone` varchar(30) DEFAULT NULL,
  `InterlocuteurId` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `statut`
--

CREATE TABLE `statut` (
  `Id` char(36) NOT NULL,
  `Libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`Id`, `Libelle`) VALUES
('1', 'prêt à publier'),
('2', 'publié');

-- --------------------------------------------------------

--
-- Structure de la table `statut_historique_rtyu5`
--

CREATE TABLE `statut_historique_rtyu5` (
  `SollicitationId` char(36) DEFAULT NULL,
  `StatutCode` char(36) DEFAULT NULL,
  `Date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sys_colonne_ref_q9kl7`
--

CREATE TABLE `sys_colonne_ref_q9kl7` (
  `Code` int(5) NOT NULL,
  `SqlName` varchar(50) DEFAULT NULL,
  `SqlNameListe` varchar(50) DEFAULT NULL,
  `Libelle` varchar(100) DEFAULT NULL,
  `Requete` varchar(5000) DEFAULT NULL,
  `Ordre` int(11) DEFAULT NULL,
  `Actif` tinyint(1) DEFAULT 1,
  `ColonneType` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `sys_colonne_ref_q9kl7`
--

INSERT INTO `sys_colonne_ref_q9kl7` (`Code`, `SqlName`, `SqlNameListe`, `Libelle`, `Requete`, `Ordre`, `Actif`, `ColonneType`) VALUES
(1, 'Code', 'ref_type_contact_ae4au', 'Code', '', 1, 1, 'CHAR'),
(2, 'Libelle', 'ref_type_contact_ae4au', 'Libellé', '', 2, 1, 'CHAR'),
(3, 'Actif', 'ref_type_contact_ae4au', 'Actif', '', 4, 1, 'BOOL'),
(16, 'Type', 'ref_type_contact_ae4au', 'Type', 'SELECT Code, Libelle from sys_type_contact_gz65fe', 3, 1, 'LIST'),
(19, 'Code', 'liste_perso_b2g5n', 'Code', NULL, 1, 1, 'CHAR'),
(20, 'Libelle', 'liste_perso_b2g5n', 'Libellé', NULL, 2, 1, 'CHAR'),
(21, 'Actif', 'liste_perso_b2g5n', 'Actif', NULL, 4, 1, 'BOOL'),
(22, 'CodeChamp', 'liste_perso_b2g5n', 'Code champ', NULL, 3, 1, 'CHAR'),
(24, 'Code', 'ref_etat_ph84p', 'Code', NULL, 1, 1, 'CHAR'),
(25, 'Libelle', 'ref_etat_ph84p', 'Libellé', NULL, 2, 1, 'CHAR'),
(26, 'SysEtatCode', 'ref_etat_ph84p', 'Etat système', 'SELECT Code, Libelle from sys_sollicitation_etat_df4gh', 3, 1, 'LIST'),
(27, 'Actif', 'ref_etat_ph84p', 'Actif', NULL, 4, 1, 'BOOL'),
(28, 'Code', 'ref_service_qhm29', 'Code', NULL, 1, 1, 'CHAR'),
(29, 'Libelle', 'ref_service_qhm29', 'Libellé', NULL, 2, 1, 'CHAR'),
(30, 'Actif', 'ref_service_qhm29', 'Actif', NULL, 3, 1, 'BOOL'),
(31, 'Code', 'ref_objet_qs6v7', 'Code', NULL, 1, 1, 'CHAR'),
(32, 'Libelle', 'ref_objet_qs6v7', 'Libellé', NULL, 2, 1, 'CHAR'),
(33, 'Actif', 'ref_objet_qs6v7', 'Actif', NULL, 3, 1, 'BOOL'),
(34, 'Code', 'ref_statut_zx8u7', 'Code', NULL, 1, 1, 'CHAR'),
(35, 'Libelle', 'ref_statut_zx8u7', 'Libellé', NULL, 2, 1, 'CHAR'),
(36, 'Actif', 'ref_statut_zx8u7', 'Actif', NULL, 3, 1, 'BOOL'),
(37, 'Defaut', 'ref_etat_ph84p', 'Defaut', NULL, 5, 1, 'BOOL');

-- --------------------------------------------------------

--
-- Structure de la table `sys_export_filter_md7g3`
--

CREATE TABLE `sys_export_filter_md7g3` (
  `Code` int(20) NOT NULL,
  `SqlName` varchar(200) DEFAULT NULL,
  `Libelle` varchar(50) DEFAULT NULL,
  `Actif` tinyint(1) NOT NULL DEFAULT 1,
  `Type` varchar(25) NOT NULL,
  `Requete` varchar(500) NOT NULL,
  `IsCP` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `sys_export_filter_md7g3`
--

INSERT INTO `sys_export_filter_md7g3` (`Code`, `SqlName`, `Libelle`, `Actif`, `Type`, `Requete`, `IsCP`) VALUES
(1, 'contact_urzkc.Adresse', 'Adresse du contact', 1, 'string', '', 0),
(2, 'contact_urzkc.CplAdresse', 'Complément d\'adresse', 1, 'string', '', 0),
(3, 'contact_urzkc.Ville', 'Ville', 1, 'string', '', 0),
(4, 'contact_urzkc.CodePostal', 'Code postal', 1, 'string', '', 0),
(13, 'contact_urzkc.Siret', 'Siret du contact', 1, 'string', '', 0),
(16, 'contact_urzkc.TypeCode', 'Type de contact', 1, 'string', '', 0),
(18, 'interlocuteurs_ry7a9.Mail', 'Adresse mail de l\'interlocuteur', 1, 'string', '', 0),
(19, 'interlocuteurs_ry7a9.Telephone', 'Numéro de téléphone de l\'interlocuteur', 1, 'string', '', 0),
(37, 't93.Valeur', 'test44', 1, 'string', 'SELECT `Code`, `Libelle`, `CodeChamp`, `Actif` FROM liste_perso_b2g5n WHERE CodeChamp = 93', 0);

-- --------------------------------------------------------

--
-- Structure de la table `sys_liste_ref_qbc4c`
--

CREATE TABLE `sys_liste_ref_qbc4c` (
  `SqlName` varchar(50) NOT NULL,
  `Libelle` varchar(100) DEFAULT NULL,
  `Requete` varchar(5000) DEFAULT NULL,
  `Actif` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `sys_liste_ref_qbc4c`
--

INSERT INTO `sys_liste_ref_qbc4c` (`SqlName`, `Libelle`, `Requete`, `Actif`) VALUES
('90', 'Est un elu', 'SELECT `Code`, `Libelle`, `CodeChamp`, `Actif` FROM liste_perso_b2g5n WHERE CodeChamp = 90', 1),
('ref_etat_ph84p', 'Liste des etats de sollicitation', 'SELECT ref_etat_ph84p.Code, ref_etat_ph84p.Libelle,sys_sollicitation_etat_df4gh.Libelle as SysEtatCode, ref_etat_ph84p.Actif, ref_etat_ph84p.Defaut FROM `ref_etat_ph84p`  inner join sys_sollicitation_etat_df4gh on sys_sollicitation_etat_df4gh.Code = SysEtatCode', 1),
('ref_objet_qs6v7', 'Liste des objets de sollicitation', 'SELECT Code, Libelle, Actif, Defaut  FROM `ref_objet_qs6v7`', 1),
('ref_service_qhm29', 'Liste des services', 'SELECT Code, Libelle, Actif, Defaut  FROM `ref_service_qhm29`', 1),
('ref_statut_zx8u7', 'Liste des statuts de sollicitation', 'SELECT Code, Libelle, Actif, Defaut  FROM `ref_statut_zx8u7`', 1),
('ref_type_contact_ae4au', 'Liste des types de contact', 'SELECT ref_type_contact_ae4au.Code, ref_type_contact_ae4au.Libelle, sys_type_contact_gz65fe.Libelle as Type, ref_type_contact_ae4au.Actif FROM ref_type_contact_ae4au INNER JOIN sys_type_contact_gz65fe on sys_type_contact_gz65fe.Code = ref_type_contact_ae4au.Type', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sys_operator_mio7h`
--

CREATE TABLE `sys_operator_mio7h` (
  `Id` int(11) NOT NULL,
  `TypeCode` varchar(50) NOT NULL,
  `Libelle` varchar(50) NOT NULL,
  `Operateur` varchar(200) NOT NULL,
  `Ordre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `sys_operator_mio7h`
--

INSERT INTO `sys_operator_mio7h` (`Id`, `TypeCode`, `Libelle`, `Operateur`, `Ordre`) VALUES
(1, 'string', 'Égal', '=', 1),
(2, 'string', 'N\'est pas égal', '!=', 2),
(3, 'string', 'Est vide', 'IS NULL', 3),
(4, 'string', 'N\'est pas vide', 'IS NOT NULL', 4),
(5, 'string', 'Contient', 'LIKE \'%$%\'', 5),
(6, 'string', 'Ne contient pas', 'NOT LIKE \'%$%\'', 6),
(7, 'string', 'Commence par', 'LIKE \'$%\'', 7),
(8, 'string', 'Termine par', 'LIKE \'%$\'', 9),
(9, 'date', 'Égal', '=', 1),
(10, 'date', 'N\'est pas égal', '!=', 2),
(11, 'date', 'Est plus petit que', '<', 3),
(12, 'date', 'Est plus grand que', '>', 4),
(13, 'date', 'Est plus petit ou égal que', '<=', 5),
(14, 'date', 'Est plus grand ou égal à', '>=', 6),
(15, 'date', 'Est vide', 'IS NULL', 7),
(16, 'date', 'N\'est pas vide', 'IS NOT NULL', 8),
(17, 'number', 'Égal', '=', 1),
(18, 'number', 'N\'est pas égal', '!=', 2),
(19, 'number', 'Est vide', 'IS NULL', 3),
(20, 'number', 'N\'est pas vide', 'IS NOT NULL', 4),
(21, 'boolean', 'Égal', '=', 1),
(22, 'boolean', 'N\'est pas égal', '!=', 2),
(23, 'boolean', 'Est vide', 'IS NULL', 3),
(24, 'boolean', 'N\'est pas vide', 'IS NOT NULL', 4),
(35, 'string', 'Ne commence pas par', 'NOT LIKE \'$%\'', 8);

-- --------------------------------------------------------

--
-- Structure de la table `sys_sollicitation_etat_df4gh`
--

CREATE TABLE `sys_sollicitation_etat_df4gh` (
  `Code` char(2) NOT NULL,
  `Libelle` varchar(100) DEFAULT NULL,
  `Actif` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `sys_sollicitation_etat_df4gh`
--

INSERT INTO `sys_sollicitation_etat_df4gh` (`Code`, `Libelle`, `Actif`) VALUES
('1', 'Non traité', 1),
('2', 'En cours', 1),
('3', 'En attente', 1),
('4', 'Résolu', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sys_type_contact_gz65fe`
--

CREATE TABLE `sys_type_contact_gz65fe` (
  `Code` char(2) NOT NULL,
  `Libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Déchargement des données de la table `sys_type_contact_gz65fe`
--

INSERT INTO `sys_type_contact_gz65fe` (`Code`, `Libelle`) VALUES
('1', 'Particulier'),
('2', 'Entreprise');

-- --------------------------------------------------------

--
-- Structure de la table `user_n5u8j`
--

CREATE TABLE `user_n5u8j` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `Mail` varchar(50) NOT NULL,
  `DateDepart` varchar(8) NOT NULL,
  `Actif` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_s4mda`
--

CREATE TABLE `utilisateur_s4mda` (
  `Id` char(36) NOT NULL,
  `Nom` varchar(100) DEFAULT NULL,
  `Prenom` varchar(100) DEFAULT NULL,
  `Initiale` char(10) DEFAULT NULL,
  `MotDePass` varchar(250) DEFAULT NULL,
  `Token` varchar(2000) DEFAULT NULL,
  `IdMotDePasseOublie` char(36) DEFAULT NULL,
  `DateMotDePasseOublie` datetime DEFAULT NULL,
  `Actif` tinyint(1) DEFAULT NULL,
  `AdresseMail` varchar(250) DEFAULT NULL,
  `DateFinContrat` varchar(15) NOT NULL DEFAULT '00002020',
  `ServiceCode` varchar(10) NOT NULL DEFAULT '1',
  `Association` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_service_g6rt4`
--

CREATE TABLE `utilisateur_service_g6rt4` (
  `ServiceCode` varchar(20) DEFAULT NULL,
  `UtilisateurId` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Valeur_CP_Contact_gf4dg`
--

CREATE TABLE `Valeur_CP_Contact_gf4dg` (
  `Code` int(4) NOT NULL,
  `ContactId` varchar(36) NOT NULL,
  `CodeChamp` int(4) NOT NULL,
  `Valeur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Valeur_CP_Sollicitation_gf4sl`
--

CREATE TABLE `Valeur_CP_Sollicitation_gf4sl` (
  `Code` int(4) NOT NULL,
  `SollicitationId` varchar(36) NOT NULL,
  `CodeChamp` int(4) NOT NULL,
  `Valeur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Administres_Reseaux_ejcj9`
--
ALTER TABLE `Administres_Reseaux_ejcj9`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CollectiviteId` (`CollectiviteId`);

--
-- Index pour la table `Administres_Utilisateurs_g6o3f`
--
ALTER TABLE `Administres_Utilisateurs_g6o3f`
  ADD PRIMARY KEY (`ContactId`),
  ADD KEY `CollectiviteId` (`CollectiviteId`) USING BTREE;

--
-- Index pour la table `champs_perso_l8f5x`
--
ALTER TABLE `champs_perso_l8f5x`
  ADD PRIMARY KEY (`Code`),
  ADD UNIQUE KEY `IdHtmlName` (`IdHtmlName`),
  ADD KEY `CollectiviteId` (`CollectiviteId`);

--
-- Index pour la table `collectivite`
--
ALTER TABLE `collectivite`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `collectivite_demarches`
--
ALTER TABLE `collectivite_demarches`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CollectiviteId` (`CollectiviteId`),
  ADD KEY `DemarcheCode` (`DemarcheCode`);

--
-- Index pour la table `contact_urzkc`
--
ALTER TABLE `contact_urzkc`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `TypeCode` (`TypeCode`);

--
-- Index pour la table `droit_d21cf`
--
ALTER TABLE `droit_d21cf`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `LieuxId` (`LieuxId`),
  ADD KEY `UtilisateurId` (`Organisateur`),
  ADD KEY `EtatId` (`EtatId`),
  ADD KEY `StatutId` (`StatutId`);

--
-- Index pour la table `groupe_g1pk5`
--
ALTER TABLE `groupe_g1pk5`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `inscription_evenement`
--
ALTER TABLE `inscription_evenement`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `EvenementId` (`EvenementId`) USING BTREE,
  ADD KEY `ContactId` (`ContactId`) USING BTREE;

--
-- Index pour la table `interlocuteurs_ry7a9`
--
ALTER TABLE `interlocuteurs_ry7a9`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `json_page_c4sd6`
--
ALTER TABLE `json_page_c4sd6`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `liens_externes`
--
ALTER TABLE `liens_externes`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CollectiviteId` (`CollectiviteId`);

--
-- Index pour la table `lieux`
--
ALTER TABLE `lieux`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `liste_champs_bn45z`
--
ALTER TABLE `liste_champs_bn45z`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `liste_perso_b2g5n`
--
ALTER TABLE `liste_perso_b2g5n`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `notification_her7u`
--
ALTER TABLE `notification_her7u`
  ADD KEY `notification_UtilisateurId` (`UtilisateurId`);

--
-- Index pour la table `ref_demandes`
--
ALTER TABLE `ref_demandes`
  ADD PRIMARY KEY (`Code`),
  ADD KEY `Type` (`Type`) USING BTREE;

--
-- Index pour la table `ref_etat_ph84p`
--
ALTER TABLE `ref_etat_ph84p`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `ref_objet_qs6v7`
--
ALTER TABLE `ref_objet_qs6v7`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `ref_rendezvous_etat`
--
ALTER TABLE `ref_rendezvous_etat`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `ref_rendezvous_objet`
--
ALTER TABLE `ref_rendezvous_objet`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `ref_service_qhm29`
--
ALTER TABLE `ref_service_qhm29`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `ref_statut_zx8u7`
--
ALTER TABLE `ref_statut_zx8u7`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `rendezvous`
--
ALTER TABLE `rendezvous`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ObjectCode` (`ObjectCode`) USING BTREE,
  ADD KEY `ContactId` (`ContactId`),
  ADD KEY `EtatCode` (`EtatCode`) USING BTREE;

--
-- Index pour la table `sib_contact_list_m1lk2`
--
ALTER TABLE `sib_contact_list_m1lk2`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `sollicitations_p55if`
--
ALTER TABLE `sollicitations_p55if`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ContactId` (`ContactId`);

--
-- Index pour la table `statut`
--
ALTER TABLE `statut`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `statut_historique_rtyu5`
--
ALTER TABLE `statut_historique_rtyu5`
  ADD KEY `statut_historique_SollicitationId` (`SollicitationId`);

--
-- Index pour la table `sys_colonne_ref_q9kl7`
--
ALTER TABLE `sys_colonne_ref_q9kl7`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `sys_export_filter_md7g3`
--
ALTER TABLE `sys_export_filter_md7g3`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `sys_liste_ref_qbc4c`
--
ALTER TABLE `sys_liste_ref_qbc4c`
  ADD PRIMARY KEY (`SqlName`);

--
-- Index pour la table `sys_operator_mio7h`
--
ALTER TABLE `sys_operator_mio7h`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `sys_sollicitation_etat_df4gh`
--
ALTER TABLE `sys_sollicitation_etat_df4gh`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `sys_type_contact_gz65fe`
--
ALTER TABLE `sys_type_contact_gz65fe`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `utilisateur_s4mda`
--
ALTER TABLE `utilisateur_s4mda`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `utilisateur_service_g6rt4`
--
ALTER TABLE `utilisateur_service_g6rt4`
  ADD KEY `Utilisateur_Service_UtilisateurId` (`UtilisateurId`);

--
-- Index pour la table `Valeur_CP_Contact_gf4dg`
--
ALTER TABLE `Valeur_CP_Contact_gf4dg`
  ADD PRIMARY KEY (`Code`);

--
-- Index pour la table `Valeur_CP_Sollicitation_gf4sl`
--
ALTER TABLE `Valeur_CP_Sollicitation_gf4sl`
  ADD PRIMARY KEY (`SollicitationId`,`CodeChamp`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `champs_perso_l8f5x`
--
ALTER TABLE `champs_perso_l8f5x`
  MODIFY `Code` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `groupe_g1pk5`
--
ALTER TABLE `groupe_g1pk5`
  MODIFY `Code` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `json_page_c4sd6`
--
ALTER TABLE `json_page_c4sd6`
  MODIFY `Id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `liste_perso_b2g5n`
--
ALTER TABLE `liste_perso_b2g5n`
  MODIFY `Code` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ref_etat_ph84p`
--
ALTER TABLE `ref_etat_ph84p`
  MODIFY `Code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `ref_objet_qs6v7`
--
ALTER TABLE `ref_objet_qs6v7`
  MODIFY `Code` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ref_service_qhm29`
--
ALTER TABLE `ref_service_qhm29`
  MODIFY `Code` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ref_statut_zx8u7`
--
ALTER TABLE `ref_statut_zx8u7`
  MODIFY `Code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `sys_colonne_ref_q9kl7`
--
ALTER TABLE `sys_colonne_ref_q9kl7`
  MODIFY `Code` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT pour la table `sys_export_filter_md7g3`
--
ALTER TABLE `sys_export_filter_md7g3`
  MODIFY `Code` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT pour la table `sys_operator_mio7h`
--
ALTER TABLE `sys_operator_mio7h`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pour la table `Valeur_CP_Contact_gf4dg`
--
ALTER TABLE `Valeur_CP_Contact_gf4dg`
  MODIFY `Code` int(4) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
